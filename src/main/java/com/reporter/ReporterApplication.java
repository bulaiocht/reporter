package com.reporter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReporterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReporterApplication.class, args);
	}

//	@Bean
//	public ApplicationRunner runner (MixSQLRepository repository){
//		return args -> {
//			Supplier<UUID> supplier = UUID::randomUUID;
//			List<UUID> list = Stream.generate(supplier).limit(10).collect(Collectors.toList());
//			List<Mix> mixes = new LinkedList<>();
//			list.forEach(uuid -> mixes.add(new Mix(null, uuid.toString())));
//			mixes.forEach(repository::save);
//		};
//	}
}
