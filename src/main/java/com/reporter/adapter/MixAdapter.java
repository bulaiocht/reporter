package com.reporter.adapter;

import com.reporter.entity.Mix;
import com.reporter.port.MixPort;
import com.reporter.repository.MixSQLRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MixAdapter implements MixPort {

    @Autowired
    private MixSQLRepository mixSQLRepository;

    @Override
    public List<Mix> findAll() {
        return mixSQLRepository.findAll();
    }

    @Override
    public Optional findById(Long id) {
        return Optional.ofNullable(mixSQLRepository.findById(id));
    }

    @Override
    public Optional save(Mix mix) {
        return Optional.ofNullable(mixSQLRepository.save(mix));
    }
}
