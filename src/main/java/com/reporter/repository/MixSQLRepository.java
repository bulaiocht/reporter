package com.reporter.repository;

import com.reporter.entity.Mix;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

//@Repository
public interface MixSQLRepository extends JpaRepository<Mix, Long> {

    List<Mix> findAll();

    <S extends Mix> S save(S s);

    Optional<Mix> findById(Long id);

}
