package com.reporter.report;

public enum ReportType {

    PDF(".pdf"),
    HTML(".html"),
    CSV(".csv");

    private String suffix;

    ReportType(String suffix) {
        this.suffix = suffix;
    }

    public String getSuffix() {
        return suffix;
    }
}
