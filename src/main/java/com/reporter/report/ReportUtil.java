package com.reporter.report;

import com.reporter.entity.Mix;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.exception.DRException;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.report;

@Component
public class ReportUtil {

//    @Autowired
//    private MixPort<Mix> mixPort;

    public File generateReport(String type) throws DRException, IOException {

        List<Mix> allMixes = Stream.generate(() -> {
            Mix mix = new Mix();
            mix.setId(Long.parseLong(RandomStringUtils.randomNumeric(10)));
            mix.setMixName(RandomStringUtils.randomAlphabetic(10).toUpperCase());
            return mix;
        }).limit(20).collect(Collectors.toList());

        Path filePath = Paths.get("src/main/resources/"+ "report" +type);

        Files.deleteIfExists(filePath);

        File file = Files.createFile(filePath).toFile();

        OutputStream outputStream = new FileOutputStream(file);

        report()
                .highlightDetailOddRows()
                .columns(
                        col.column("ID", "id", DynamicReports.type.longType()),
                        col.column("NAME", "mixName", DynamicReports.type.stringType())
                )
                .setDataSource(allMixes)
                .title(cmp.text("Test report"))
                .pageFooter(cmp.text(new Date()))
        .toPdf(outputStream);
        return file;
    }
}

