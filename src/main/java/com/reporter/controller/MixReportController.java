package com.reporter.controller;

import com.reporter.report.ReportType;
import com.reporter.report.ReportUtil;
import net.sf.dynamicreports.report.exception.DRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller("/report")
public class MixReportController {

    @Autowired
    private ReportUtil reportUtil;

    @GetMapping
    public ResponseEntity<String> getReport(){
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @GetMapping("/reports/{type}")
    public ResponseEntity<String> getReport(@PathVariable String type, HttpServletResponse response){

        try {

            File file = reportUtil.generateReport(ReportType.valueOf(type.toUpperCase()).getSuffix());

            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());

            OutputStream out = response.getOutputStream();
            Path filePath = Paths.get(file.getPath());
            Files.copy(filePath, out);
            out.flush();

        } catch (DRException | FileNotFoundException e) {
            return new ResponseEntity<>("Server error", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IllegalArgumentException e){
            return new ResponseEntity<>("No such report type", HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            return new ResponseEntity<>("IO Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>("Report of type " + type + " has been created", HttpStatus.OK);
    }

}
