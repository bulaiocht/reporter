package com.reporter.port;

import com.reporter.entity.Mix;

import java.util.List;
import java.util.Optional;

public interface MixPort<T extends Mix> {

    List<T> findAll();

    Optional<T> findById(Long id);

    Optional<T> save (T t);

}
